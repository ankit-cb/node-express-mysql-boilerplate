import mysql from 'mysql2';

// Create the connection pool. The pool-specific settings are the defaults
const pool = mysql.createPool({
  host: 'localhost',
  user: 'admin',
  password: 'password',
  database: 'test',
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0
});

pool.on('connection', (con) => {
  console.log('on connection:: ');
});
pool.on('error', (err) => {
  console.log('on error:: ', err);
});

export default pool;