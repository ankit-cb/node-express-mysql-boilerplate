import * as jwt from 'jsonwebtoken'
import { user as User } from './../models'

// const JWT_SECRET = process.env.JWT_SECRET
const JWT_SECRET = "dj9a7fhqa874wbfaw"

export const verify = async (req, res, next) => {
  const token = req.headers['authorization']

  if(!token) {
    return res
      .status(401)
      .send('Unauthorized')
  }

  try {
    const decoded = jwt.verify(token, JWT_SECRET)
    const user    = await User.findOne({ where: { id: decoded.userId }})

    if(!user) {
      return res
        .status(401)
        .send('Unauthorised')  
    }

    req.user = user
    return next()
  
  } catch(err) {
    return res
      .status(500)
      .send('Something went wrong')
  }
}

export const sign = (payload) => {
  return jwt.sign(payload, JWT_SECRET)
}