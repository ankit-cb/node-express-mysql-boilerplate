import express from 'express';
import UserController from './../controllers/User.controller.js';

const router = express.Router();

router.post('/user/login', UserController.login);
router.post('/user/register', UserController.signup);

export default router;
